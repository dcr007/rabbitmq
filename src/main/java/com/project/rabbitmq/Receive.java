package com.project.rabbitmq;
import java.io.IOException;
import java.util.Date;


import com.rabbitmq.client.*;

public class Receive {
	private final static String QUEUE_NAME = "hello";
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException,java.lang.InterruptedException {
		// TODO Auto-generated method stub
		ConnectionFactory factory = new ConnectionFactory();
	    factory.setHost("localhost");
	    Connection connection = factory.newConnection();
	    Channel channel = connection.createChannel();

	    channel.queueDeclare(QUEUE_NAME, false, false, false, null);
	    System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
	    
	    Consumer consumer = new DefaultConsumer(channel) {
	    	 long startTime = new Date().getTime();
	    	@Override
	        public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
	            throws IOException {
	          String message = new String(body, "UTF-8");
	          long endTime = new Date().getTime();
	          System.out.println(" [x] Received '" + message + "'"+"of Size"+ (message.getBytes("UTF-8").length)/1000 + "KB" +"in"+(endTime-startTime)+"milliseconds \n");
	        }
	      };
	      channel.basicConsume(QUEUE_NAME, true, consumer);
	}

}
