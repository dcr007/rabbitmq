package com.project.rabbitmq;

import java.util.Date;
import java.util.Random;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;

public class Send {
public static final String QUEUE_NAME="hello";
	// no of iterations to run.
	static int  itr_count =100;
	/**
	 * @param args
	 */
	public static void main(String[] argv) throws java.io.IOException{
		ConnectionFactory factory = new ConnectionFactory();
	    factory.setHost("localhost");
	    Connection connection = factory.newConnection();
	    Channel channel = connection.createChannel();
		// TODO Auto-generated method stub
	    channel.queueDeclare(QUEUE_NAME, false, false, false, null);
	    String message = "message";
	    
	    // Generates Random int  from 0 to 1024 , this represents the data sizes that has to be used. 
	    
	    
	    while (itr_count != 0 ){
	    	itr_count --;
		    Random generator = new Random();    
		    int i = generator.nextInt(1024/2) *2 ;
		    // Generates Messages of varying sizes 
		    message=createDataSize(i);    
		    long startTime = new Date().getTime();
		    channel.basicPublish("", QUEUE_NAME, null, message.getBytes());
		    long endTime = new Date().getTime();
		    System.out.println(" [Itreation --"+itr_count+"]"+"Sent" + message + "'"+"Size: "+ i +"KB"+ "in"+ (endTime-startTime) + "milli seconds \n" );
	    }
	    channel.close();
	    connection.close();
	}
	
	private static String createDataSize(int msgSize) {
	    // Java chars are 2 bytes
	    msgSize = msgSize/2;
	    msgSize = msgSize * 1024;
	    StringBuilder sb = new StringBuilder(msgSize);
	    for (int i=0; i<msgSize; i++) {
	        sb.append('a');
	    }
	    return sb.toString();
	  }
}
